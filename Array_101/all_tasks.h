#ifndef ALL_TASKS_H
#define ALL_TASKS_H
/* task_1 */ void maxConsecutiveOnes(std::vector<int> ar);
/* task_2 */ void findNumbersWithEvenNumberOfDigits(std::vector<int> ar, int key);
/* task_3 */ void squaresOfSortedArray(std::vector<int> ar);
/* task_4 */ void duplicateZeros(std::vector<int> ar);
/* task_5 */ void mergeSortedArray(std::vector<int> lAr, int lArSize, std::vector<int> rAr, int rArSize);
/* task_6 */ int removeElement(std::vector<int> ar, int key);
#endif // ALL_TASKS_H
