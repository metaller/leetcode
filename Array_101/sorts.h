#ifndef SORTS_H
#define SORTS_H
void countingSort(int* ar, int size, int maxElement);
void quickSort(int* ar, int size);
void mergeSort(int* ar, int size);
#endif // SORTS_H
