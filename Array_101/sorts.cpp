#include <utility>
#include <cstring>
#include <vector>
#include "sorts.h"

void fillZeros(std::vector<int> counter, int maxElement) {
    for(int i = 0; i < maxElement; i++) {
       counter[i] = 0;
    }
}

void countingSort(int* ar, int size, int maxElement) {
    std::vector<int> counter;
    fillZeros(counter, maxElement);
    for(int i = 0; i < size; i++) {
        counter[ar[i]]++;
    }
    int position = 0;
    for(int i = 0; i < maxElement; i++) {
        for(int j = 0; j < counter[i]; j++) {
            ar[position++] = i;
        }
    }
}

void quickSort(int* ar, int size) {
    int left = 0;
    int right = size - 1;
    int pivot = ar[size >> 1];
    while(left <= right) {
        while(ar[left] < pivot) {
            left++;
        }
        while(ar[right] > pivot) {
            right--;
        }
        if(left <= right) {
            std::swap(ar[right], ar[left]);
            right--;
            left++;
        }
    }
    if(right > 0) {
        quickSort(&ar[0], right + 1);
    }
    if(left < size) {
        quickSort(&ar[left], size - left);
    }
}

void merge(int* ar, int size, int central) {
    int left = 0;
    int right = central;
    int* arTemp = new int [size];
    int indexTemp = 0;
    while(left < central && right < size) {
        while(ar[left] <= ar[right] && left < central) {
            arTemp[indexTemp++] = ar[left++];
        }
        while(ar[left] > ar[right] && right < size) {
            arTemp[indexTemp] = ar[right];
            indexTemp++;
            right++;
        }
    }
    while(left < central) {
        arTemp[indexTemp++] = ar[left++];
    }
    while(right < size) {
        arTemp[indexTemp++] = ar[right++];
    }
    std::memcpy(ar, arTemp, size * sizeof(int));
    delete [] arTemp;
}

void mergeSort(int* ar, int size) {
    if (size <= 1) {
        return;
    }
    mergeSort(&ar[0], size/2);
    mergeSort(&ar[size/2], size - (size/2));
    merge(&ar[0], size/2, size/2);
}
