#include <utility>
#include <vector>
#include "all_tasks.h"
#include "sorts.h"

/*  task_1
    ------
    - task: найти максимально количество единиц последовательности N чисел (0 <= N <= 1),
    идущих подряд
    - solution method: выполняем единичный проход по последовательности чисел,
    если при проходе встречается элемент равный нулю, то переходим на следующий
    элемент, *... . как только встречается такой ar[i], который равен единице
    увеличиваем значение переменной unit_counter(счётчик единиц) на единицу и
    переходим к проверке следующего элемента. *добавляем текущее значение переменной
    unit_counter в вектор counter и обнуляем её значение. в итоге мы получим,
    вероятно неотсортированную, последовательности чисел, которую надо отсотировать
    и узнать значение последнего элемента
    - solution date: 23.02.23
    - author: Andrey Bezus
*/

void maxConsecutiveOnes(std::vector<int> ar) {
    std::vector<int> counter;
    for(int i = 0; i < ar.size(); i++) {
        int unit_counter = 0;
        while(ar[i] != 0 && i < ar.size()) {
            unit_counter++;
            i++;
        }
        counter.push_back(unit_counter);
    }
    quickSort(&counter[0], counter.size());
}

/* task_2
   ------
    - task: найти количество чисел последовательности, сумма цифр которых кратна N,
    где N - целое неотрицательное число
    - solution method:
    - solution date: 24.02.23
    - author: Andrey Bezus
*/

void findNumbersWithEvenNumberOfDigits(std::vector<int> ar, int key) {
    std::vector<int> counter;
    for(int i = 0; i < ar.size(); i++) {
        int countOfDigitInNum = 0;
        if(ar[i] == 0) {
            counter.push_back(1);
            continue;
        }
        while(ar[i] != 0) {
            ar[i] /= 10;
            countOfDigitInNum++;
        }
        counter.push_back(countOfDigitInNum);
    }
    int countOfDesiredNums = 0;
    for(int i = 0; i < counter.size(); i++) {
        while(counter[i] % key == 0 && i < counter.size()) {
            countOfDesiredNums++;
            i++;
        }
    }
}

/* task_3
   ------
    - task:
    - solution method:
    - solution date: 25.02.23
    - author: Andrey Bezus
*/

void squaresOfSortedArray(std::vector<int> ar) {
    quickSort(&ar[0], ar.size());
    std::vector<int> allInSquare;
    for(int i = 0; i < ar.size(); i++) {
        allInSquare.push_back(ar[i] * ar[i]);
    }
    quickSort(&allInSquare[0], ar.size());
}

/* task_4
   ------
    - task:
    - solution method:
    - solution date: 26.02.23
    - author: Andrey Bezus
*/

void duplicateZeros(std::vector<int> ar) {
    std::vector<int> someName;
    for(int i = 0; i < ar.size(); i++) {
        someName.push_back(ar[i]);
        if(ar[i] == 0) {
            someName.push_back(0);
        }
    }
    int i = someName.size();
    while(i != ar.size()) {
        someName.pop_back();
        i--;
    }
}

/* task_5
   ------
    - task:
    - solution method:
    - solution date: 27.02.23
    - author: Andrey Bezus
*/

void mergeSortedArray(std::vector<int> lAr, int lArSize, std::vector<int> rAr, int rArSize) {
    int i = 0;
    int j = 0;
    int* iAr = new int;
    while(i < lArSize && j < rArSize) {
        if(lAr[i] <= rAr[j]) {
            iAr[i + j] = lAr[i];
            i++;
        }
        if(rAr[j] <= lAr[i]) {
            iAr[j + i] = rAr[j];
            j++;
        }
    }
    while(i < lArSize) {
        iAr[i + j] = lAr[i];
        i++;
    }
    while(j < rArSize) {
        iAr[j + i] = rAr[j];
        j++;
    }
}

/* task_6
   ------
    - task:
    - solution method:
    - solution date:
    - author: Andrey Bezus
*/

int removeElement(std::vector<int> ar, int key) {

}
