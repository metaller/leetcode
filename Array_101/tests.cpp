#include <iostream>
#include <vector>
#include "tests.h"
#include "all_tasks.h"
#include "sorts.h"

void test_MaxConsecutiveOnes() {
    { /* test_1 */
        std::vector<int> ar{1,1,0,1,1,1};
        maxConsecutiveOnes(ar);
    }
    { /* test_2 */
        std::vector<int> ar{1,0,1,1,0,1};
        maxConsecutiveOnes(ar);
    }
    { /* test_3 */
        std::vector<int> ar{1,1,1,1,1,0,1,0,1,1};
        maxConsecutiveOnes(ar);
    }
    { /* test_4 */
        std::vector<int> ar{0,0};
        maxConsecutiveOnes(ar);
    }
    std::cout << "Task №1 - OK!" << std::endl;
}

void test_FindNumbersWithEvenNumberOfDigits() {
    { /* test_1 */
        std::vector<int> ar{12,345,2,6,7896};
        findNumbersWithEvenNumberOfDigits(ar, 2);
    }
    { /* test_2 */
        std::vector<int> ar{555,901,482,1771};
        findNumbersWithEvenNumberOfDigits(ar, 2);
    }
    { /* test_3 */
        std::vector<int> ar{6,24,543,12,0,3734};
        findNumbersWithEvenNumberOfDigits(ar, 3);
    }
    { /* test_4 */
        std::vector<int> ar{19,4387,298,0,321};
        findNumbersWithEvenNumberOfDigits(ar, 4);
    }
    std::cout << "Task №2 - OK!" << std::endl;
}

void test_SquaresOfSortedArray() {
    { /* test_1 */
        std::vector<int> ar{-4,-1,0,3,10};
        squaresOfSortedArray(ar);
    }
    { /* test_2 */
        std::vector<int> ar{-7,-3,2,3,11};
        squaresOfSortedArray(ar);
    }
    { /* test_3 */
        std::vector<int> ar{-8,-6,-2,1,12};
        squaresOfSortedArray(ar);
    }
    { /* test_4 */
        std::vector<int> ar{-5,0,2,13,21};
        squaresOfSortedArray(ar);
    }
    std::cout << "Task №3 - OK!" << std::endl;
}

void test_DuplicateZeros() {
    { /* test_1 */
        std::vector<int> ar{1,0,2,3,0,4,5,0};
        duplicateZeros(ar);
    }
    { /* test_2 */
        std::vector<int> ar{1,0,0,0,2,5,0};
        duplicateZeros(ar);
    }
    { /* test_3 */
        std::vector<int> ar{0,0,0,3,4,0};
        duplicateZeros(ar);
    }
    { /* test_4 */
        std::vector<int> ar{1,2,3,4,5,0};
        duplicateZeros(ar);
    }
    std::cout << "Task №4 - OK!" << std::endl;
}

void test_MergeSortedArray() {
    { /* test_1 */
        std::vector<int> lAr{1,2,3,0,0,0};
        std::vector<int> rAr{2,5,6};
        mergeSortedArray(lAr, 3, rAr, 3);
    }
    { /* test_2 */
        std::vector<int> lAr{1};
        std::vector<int> rAr{};
        mergeSortedArray(lAr, 1, rAr, 0);
    }
    { /* test_3 */
        std::vector<int> lAr{0};
        std::vector<int> rAr{1};
        mergeSortedArray(lAr, 0, rAr, 1);
    }
    { /* test_4 */
        std::vector<int> lAr{0,2,5,0};
        std::vector<int> rAr{1,5,6};
        mergeSortedArray(lAr, 3, rAr, 1);
    }
    std::cout << "Task №5 - OK!" << std::endl;
}
