#include "tests.h"

int main() {
    test_MaxConsecutiveOnes();
    test_FindNumbersWithEvenNumberOfDigits();
    test_SquaresOfSortedArray();
    test_DuplicateZeros();
    test_MergeSortedArray();
    return 0;
}
