#ifndef TESTS_H
#define TESTS_H
/* TEST №1 for task_1 */ void test_MaxConsecutiveOnes();
/* TEST №2 for task_2 */ void test_FindNumbersWithEvenNumberOfDigits();
/* TEST №3 for task_3 */ void test_SquaresOfSortedArray();
/* TEST №4 for task_4 */ void test_DuplicateZeros();
/* TEST №5 for task_5 */ void test_MergeSortedArray();
#endif // TESTS_H 
